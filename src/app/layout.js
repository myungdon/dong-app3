'use client'
//  use client 선언하면 메타데이터를 쓸 수 없다
//  메타데이터 - 바뀌는 않는 중요한 파일
//  레이아웃은 계쏙 바뀐다
//  use client 안쪽에 있는 페이집니다 라고 말하는 거임 ( 메타 데이터와 함께쓰면 충돌이 난다 )
import {Provider} from "react-redux";
import {Inter} from "next/font/google";
import "./globals.css";
import store from "@/lib/store";

const inter = Inter({subsets: ["latin"]});

export default function RootLayout({children}) {
    return (
        <html lang="en">
        <Provider store={store}>
            <body className={inter.className}>{children}</body>
        </Provider>
        </html>
    );
}

// 레이아웃으로 스토어를 제공한다
// 그래서 스토어를 셋팅 해줘야 한다