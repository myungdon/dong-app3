'use client'

import { useSelector } from 'react-redux';

export default function Result() {
    const { votes } = useSelector((state) => state.vote);

    const getWinner = (kind) => {
        let winner = '';
        let maxVotes = 0;
        for (const student in votes) {
            if (votes[student][kind] > maxVotes) {
                winner = student;
                maxVotes = votes[student][kind];
            }
        }
        return winner;
    };

    return (
        <div>
            <h2>긍정질문에 가장 득표수가 많은 사람: {getWinner('GOOD')}</h2>
            <h2>부정질문에 가장 득표수가 많은 사람: {getWinner('BAD')}</h2>
        </div>
    );
}