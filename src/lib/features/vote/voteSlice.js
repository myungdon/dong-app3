import { createSlice } from '@reduxjs/toolkit';
import {reducer} from "next/dist/client/components/router-reducer/router-reducer";

const initialState = {
    students: ['김길동', '홍길동', '나길동'],
    questions: [
        { kind: 'GOOD', title: '책상이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '책상이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '주변이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '옷이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '가방이 가장 깨끗한 학생은?' }
    ],
    votes: {},
    currentQuestionIndex: 0,
    selections: {},
};
// 키 밸류 형태로 초기값을 넣어줌

const voteSlice = createSlice({
    name: 'vote',
    // 이름을 파일명과 맞춘다
    initialState,
    reducers: {     // 모든 행위는 리듀서만 한다 ( 바꾸기, 삭제 )
        // 왜 리듀서가 값을 조작 해야 할까? (값을 변경하는 함수)
        // → 페이지에서 값을 조작하면 바뀔때마다 다 말해야함 and 버그 났을 때 누구 오류인지 모름
        select: (state, action) => {
            const { student, question } = action.payload;
            if (!state.selections[student]) {
                state.selections[student] = { GOOD: [], BAD: [] };
            }
            state.selections[student][question.kind].push(question);
        },
        nextQuestion: (state) => {
            for (const student in state.selections) {
                if (!state.votes[student]) {
                    state.votes[student] = { GOOD: 0, BAD: 0 };
                }
                state.votes[student].GOOD += state.selections[student].GOOD.length;
                state.votes[student].BAD += state.selections[student].BAD.length;
            }
            state.selections = {};
            state.currentQuestionIndex++;
        },
    },
});

export const { select, nextQuestion } = voteSlice.actions;
//  내보내겠다
//  리듀서의 자리가 actions의 자리임
//  export const와 export default의 차이?
//  한파일에서 여러번 뽑을 수 있다. 다만 기본은 한번뿐
//  이니셜스테이트 기본값을 가지고 셀렉트 넥스트퀘스천을 가지고 이름이 vote인 객체를 만든 것이다
//  함수도 객체이기 때문에 프로퍼티로 함수안에 함수를 담은 것이다.
//  그래서 언패킹 하니까 셀렉트와 넥스트퀘스쳔이 나오게 됨
//  export const 스코프를 통해 빼낼수 잇도록 해줌

export default voteSlice.reducer;