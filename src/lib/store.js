import { configureStore } from '@reduxjs/toolkit';
import voteReducer from './features/vote/voteSlice';
// 슬라이스를 만들었으니까 스토어에 알려줘야 함
// 받을 때 기본으로 주는 이름을 정할수 있다.
//  임포트에서 {}와 없는것의 차이는? 임포트에서 스코프 처리하면 원하는 함수만 빼오는 것 없으면 기본으로 주는 것받아서 쓰겠다 따라서 이름 바꿀 수 있다.
//  임포트를 하는 곳이면 다 이렇게 쓴다.
export default configureStore({
    reducer: {
        vote: voteReducer,
    },
});
//  키도 바꿀수 있다.
//  그러나 나중에 추적이 안되니까 모듈이름과 맞춰준다